import React from 'react';
import {useDispatch} from "react-redux";
import PhotoAddForm from "../../components/AddPhotoForm/PhotoAddForm";
import {postNewResto} from "../../store/actions/restaurantsActions";

const AddNewRestoPage = () => {
  const dispatch = useDispatch()

  const createRestoImage = (data) => {
    dispatch(postNewResto(data));
  };

  return (
      <>
        <PhotoAddForm onSubmit={createRestoImage}/>
      </>
  );
};

export default AddNewRestoPage;