import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addNewImage, getSingleResto} from "../../store/actions/restaurantsActions";
import {CardMedia, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {apiUrl, defaultAvatar} from "../../constants";
import CommentAndRating from "../../components/CommentAndRating/CommentAndRating";
import AddNewImage from "../../components/AddNewImage/AddNewImage";

const useStyle = makeStyles(() => ({
  resto_desc:{
    fontSize: "20px"
  },
  mainPhoto:{
    height: "180px",
    width: "50%",
  },
  main_info:{
    display: "flex"
  }
}))

const RestoPage = props => {
  const classes = useStyle();
  const singleResto = useSelector(state => state.restaurants.singleRestaurant);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getSingleResto(props.match.params.id))
  }, [dispatch, props.match.params.id]);
  
  let newPhoto = defaultAvatar;
  if(singleResto.photo){
    newPhoto = apiUrl + "/uploads/" + singleResto.photo;
  };

  const AddNewImageFromResto = (data) => {
    dispatch(addNewImage(data))
  };


  return (
      <div>
        <div className={classes.main_info}>
          <div>
            <Typography variant="h2">{singleResto.title}</Typography>
            <p className={classes.resto_desc}>{singleResto.description}</p>
          </div>
          <CardMedia className={classes.mainPhoto} image={newPhoto}/>
        </div>
        <div>
          Gallery will be here!
        </div>
        <CommentAndRating/>
        <AddNewImage onSubmit={AddNewImageFromResto}/>
      </div>
  );
};

export default RestoPage;