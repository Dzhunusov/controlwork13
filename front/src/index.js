import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import axiosApi from "./axiosApi";
import store, {history} from "./store/configureStore";
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";

axiosApi.interceptors.request.use(config => {
  try{
    config.headers["Authorization"] = store.getState().users.user.token;
  }catch (e) {
    // DO NOTHING, NO TOKEN EXISTS
  }
  return config;
});

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
