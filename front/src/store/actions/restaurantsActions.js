import {
  ADD_NEW_IMAGE_FROM_RESTO,
  GET_RESTAURANTS_LIST_FAILURE,
  GET_RESTAURANTS_LIST_SUCCESS,
  GET_SINGLE_RESTAURANT_FAILURE,
  GET_SINGLE_RESTAURANT_SUCCESS,
  POST_NEW_RESTAURANT, PUT_QUALITY_TO_RESTO_FAILURE,
  PUT_QUALITY_TO_RESTO_SUCCESS
} from "../actionsTypes";
import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

const getRestaurantListSuccess = (restoList) => {
  return {type: GET_RESTAURANTS_LIST_SUCCESS, restoList};
};

const getRestaurantListFailure = (error) => {
  return {type: GET_RESTAURANTS_LIST_FAILURE, error};
};

export const getRestoList = () => {
  return async dispatch => {
    try{
      const response = await axiosApi.get("/restaurants");
      dispatch(getRestaurantListSuccess(response.data));
    }catch (e){
      dispatch(getRestaurantListFailure(e.response));
    }
  }
};

const getSingleRestaurantSuccess = (restoData) => {
  return {type: GET_SINGLE_RESTAURANT_SUCCESS, restoData};
};

const getSingleRestaurantsFailure = (error) => {
  return {type: GET_SINGLE_RESTAURANT_FAILURE, error};
};

export const getSingleResto = (restoId) => {
  return async dispatch => {
    try{
      const response = await axiosApi.get("/restaurants/" + restoId);
      dispatch(getSingleRestaurantSuccess(response.data));
    } catch (e){
      dispatch(getSingleRestaurantsFailure(e.response));
    }
  }
};

const postNewRestaurant = () => {
  return {type: POST_NEW_RESTAURANT};
};

export const postNewResto = (data) => {
  return (dispatch, getState) => {
    const headers = {"Authorization" : getState().users.user.token};
    return axiosApi.post("/restaurants", data, headers).then(() => {
      dispatch(postNewRestaurant());
      dispatch(push("/"));
    })
  }
};

const putQualityToRestoSuccess = () => {
  return {type: PUT_QUALITY_TO_RESTO_SUCCESS};
};

const putQualityToRestoFailure = error => {
  return {type: PUT_QUALITY_TO_RESTO_FAILURE, error};
};

export const postRestoQuality = (data) => {
  return async dispatch => {
    try{
      await axiosApi.post("/quality/", data);
      dispatch(putQualityToRestoSuccess());
    } catch (e){
      dispatch(putQualityToRestoFailure(e));
    }
  }
}

const addNewImageFromResto = () => {
  return {type: ADD_NEW_IMAGE_FROM_RESTO};
};


export const addNewImage = (imageData) => {
  return (dispatch, getState) => {
    const headers = {"Authorization" : getState().users.user.token};
    return axiosApi.post("/gallerys", imageData, headers).then(() => {
      dispatch(addNewImageFromResto());
    })
  }
};



