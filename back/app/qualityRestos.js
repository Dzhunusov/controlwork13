const router = require("express").Router();
const qualityResto = require('../models/QualityResto');
const auth = require("../middleware/auth");

router.post("/",[auth], async (req, res) => {
  const qualityData = req.body;
  const newQuality = new qualityResto(qualityData);
  try{
    await newQuality.save();
    res.send(newQuality);
  }catch (e){
    res.sendStatus(404);
  }
});

module.exports = router;