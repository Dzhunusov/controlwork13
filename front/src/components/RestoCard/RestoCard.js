import React from 'react';
import {Card, CardActionArea, CardActions, CardContent, CardMedia} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/styles";
import {apiUrl, defaultAvatar} from "../../constants";

const useStyles = makeStyles({
  root: {
    maxWidth: "33.3333%",
    margin: "15px"
  },
  media: {
    height: 140,
    width: "100%"
  },
  cardFutter:{
    display: "flex"
  }
});

const RestoCard = ({title, description, image, id}) => {
  const classes = useStyles();

  let newImage = defaultAvatar;
  if(image){
    newImage = apiUrl + "/uploads/" + image;
  };


  return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
              className={classes.media}
              image={newImage}
              component={Link}
              to={"/restaurants/" + id}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component={Link} to={"/restaurants/" + id}>
              {title}
            </Typography>
            <Typography variant="h6" color="textSecondary" component="p">
              {description}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions className={classes.cardFutter}>
          <p>Rating: </p>
          <p>Photos: </p>
        </CardActions>
      </Card>
  );
};

export default RestoCard;