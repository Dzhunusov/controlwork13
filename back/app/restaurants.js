const router = require("express").Router();
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const Restaurant = require("../models/Restaurant");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const path = require('path');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadsPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
  try{
    const restaurants = await Restaurant.find().populate("User");
    res.send(restaurants);
  }catch (e){
    res.status(500).send(e);
  }
});

router.post("/", [auth], upload.single("photo"), async (req, res) => {
  const restaurantData = req.body;
  if(restaurantData.check === false){
    return res.status(400).send({error: 'You have to enable the checkbox'});
  };

  if(req.file){
    restaurantData.photo = req.file.filename;
  }
  const restaurant = new Restaurant(restaurantData);
  try{
    await restaurant.save();
    res.send(restaurant);
  }catch (e){
    res.status(404).send(e)
  }
});

router.get("/:id", async (req, res) => {
  try{
    const singleResto = await Restaurant.findById(req.params.id);
    res.send(singleResto);
  } catch (e){
    res.status(500).send(e);
  }
});

router.put("/edit/:id", async (req, res) => {
  const editedQuestion = req.body;
  try{
    const result = await Restaurant.findByIdAndUpdate(req.params.id, editedQuestion);
    if(result){
      res.send("Success");
    }else {
      res.sendStatus(400);
    }
  }catch (e){
    res.sendStatus(404);
  }
})


module.exports = router;