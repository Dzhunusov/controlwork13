const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RestaurantSchema = new Schema({
  title:{
    type: String,
    required: true,
  },
  user:{
    type: Schema.Types.ObjectId,
    ref: "User",
  },
  photo: String,
  description: String,
  check:{
    type: Boolean,
    required: true,
  },
  gallery:{
    type: Array,
  },
  quality:{
    type: Number,
  }
});

const Restaurant = mongoose.model("Restaurant", RestaurantSchema);
module.exports = Restaurant;