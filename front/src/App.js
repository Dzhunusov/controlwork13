import {useSelector} from "react-redux";
import {Container, CssBaseline} from "@material-ui/core";
import AppToolbar from "./components/UI/AppToolbar";
import {Route, Switch} from "react-router";
import MainPage from "./containers/MainPage/MainPage";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import RestoPage from "./containers/RestoPage/RestoPage";
import AddNewRestoPage from "./containers/AddNewRestoPage/AddNewRestoPage";

function App() {
  const user = useSelector(state => state.users.user);

  return (
    <>
      <CssBaseline/>
      <AppToolbar user={user}/>
      <main>
        <Container>
          <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/restaurants/:id" exact component={RestoPage}/>
            <Route path="/new_place" exact component={AddNewRestoPage}/>
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;
