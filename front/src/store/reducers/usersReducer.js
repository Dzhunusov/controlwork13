import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER, LOGOUT_USER_ERROR,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actionsTypes";

const initialState = {
  user: null,
  loginError: null,
  registerError: null,
  logoutError: null,
};

const usersReducer = (state = initialState, action) => {
  switch (action.type){
    case REGISTER_USER_SUCCESS:
      return {...state, user: action.value, registerError: null, loginError: null};
    case REGISTER_USER_FAILURE:
      return {...state, registerError: action.error};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null, registerError: null};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case LOGOUT_USER:
      return {...state, user: null};
    case LOGOUT_USER_ERROR:
      return {...state, logoutError: action.error};
    default:
      return state;
  }
};

export default usersReducer;