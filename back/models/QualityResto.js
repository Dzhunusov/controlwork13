const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const QualityRestoSchema = new Schema({
  comment:{
    type: String,
    required: true,
  },
  quality:{
    type: Number,
    required: true,
  },
  user:{
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  restaurant: {
    type: Schema.Types.ObjectId,
    ref: "Restaurant",
    required: true
  }
});

const QualityResto = mongoose.model("QualityResto", QualityRestoSchema);
module.exports = QualityResto;