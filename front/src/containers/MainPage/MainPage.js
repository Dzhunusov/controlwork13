import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getRestoList} from "../../store/actions/restaurantsActions";
import RestoCard from "../../components/RestoCard/RestoCard";
import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

const useStyle = makeStyles(() => ({
  cards: {
    display: "flex",
    flexWrap: "wrap",
    flexGrow: 1,
  },
}))

const MainPage = () => {
  const classes = useStyle();
  const restaurants = useSelector(state => state.restaurants.restaurants);
  const dispatch = useDispatch();

  useEffect(()=> {
    dispatch(getRestoList());
  }, [dispatch]);



  return (
      <div>
        <Typography variant="h2">All Places</Typography>
        <div className={classes.cards}>
          {restaurants && restaurants.map(item => {
            return <RestoCard
                key={item._id}
                image={item.photo}
                title={item.title}
                description={item.description}
                id={item._id}
            />
          })}
        </div>
      </div>
  );
};

export default MainPage;