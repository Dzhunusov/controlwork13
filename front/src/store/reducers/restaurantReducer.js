import {
  GET_RESTAURANTS_LIST_FAILURE,
  GET_RESTAURANTS_LIST_SUCCESS,
  GET_SINGLE_RESTAURANT_FAILURE,
  GET_SINGLE_RESTAURANT_SUCCESS
} from "../actionsTypes";

const initialState = {
  restaurants: [],
  restoListError: null,
  singleRestaurant: [],
  singleRestoError: null,
  restoQuality: [],
};

const restaurantReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_RESTAURANTS_LIST_SUCCESS:
      return {...state, restaurants: action.restoList};
    case GET_RESTAURANTS_LIST_FAILURE:
      return {...state, restoListError: action.error};
    case GET_SINGLE_RESTAURANT_SUCCESS:
      return {...state, singleRestaurant: action.restoData};
    case GET_SINGLE_RESTAURANT_FAILURE:
      return {...state, singleRestoError: action.error};
    default:
      return state;
  }
};

export default restaurantReducer;
    