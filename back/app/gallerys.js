const router = require("express").Router();
const Gallery = require('../models/Gallery');
const auth = require("../middleware/auth");
const config = require("../config");
const permit = require('../middleware/permit');
const path = require('path');
const multer = require('multer');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadsPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post("/:id", [auth], upload.single("image"),  async (req, res) => {
  const newImageData = req.body;
  if(req.file){
    newImageData.image = req.file.filename;
  }
  const newImages = new Gallery(newImageData);
  try{
    await newImages.save();
    res.send(newImages);
  }catch (e){
    res.status(404).send(e);
  }
})

module.exports = router;