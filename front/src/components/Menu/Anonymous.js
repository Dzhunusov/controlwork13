import React from 'react';
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";

const Anonymous = () => {
  return (
      <>
        <Button color="inherit" component={NavLink} to="/register">Sign Up</Button>
        <Button color="inherit" component={NavLink} to="/login">Sign In</Button>
      </>
  );
};

export default Anonymous;