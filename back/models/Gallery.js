const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const GallerySchema = new Schema({
  user:{
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  restaurant: {
    type: Schema.Types.ObjectId,
    ref: "Restaurant",
    required: true
  },
  image:String,
});

const Gallery = mongoose.model("Gallery", GallerySchema);
module.exports = Gallery;