const mongoose = require("mongoose");
const config = require("./config");
const {nanoid} = require("nanoid");
const Restaurant = require("./models/Restaurant");
const User = require('./models/User');

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

db.once("open", async () => {
  try{
    await db.dropCollection("restaurants");
    await db.dropCollection("images");
    await db.dropCollection("users");
  }catch (e) {
    console.log("Collection were not presented, skipping drop...");
  }

  const [user1, admin, user2] = await User.create({
    username: "User1",
    email: "user@gmail.com",
    password: "12345678Aa",
    token: nanoid(),
    role: "user"
  },{
    username: "Admin",
    email: "admin@gmail.com",
    password: "12345678Aa",
    token: nanoid(),
    role: "admin"
  },{
    username: "User2",
    email: "user2@gmail.com",
    password: "12345678Aa",
    token: nanoid(),
    role: "user"
  });

  const [LaRusse, Лингуини] = await Restaurant.create({
    title: "La Russe",
    user: user1._id,
    photo: "aLaRusse.jpeg",
    description: "Один и лучших рестроранов в Бишкеке. Исключительно вкусная русская кухня."
  },{
    title: "Лингуини",
    user: user2._id,
    description: "Изысканная французская кухня. Единственное место для самых отьявленных гурманов."
  });


  db.close();
})