import React, {useState} from 'react';
import FormField from "../UI/Form/FormField";
import FormControl from "@material-ui/core/FormControl";
import {InputLabel, MenuItem, Select} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import {useDispatch} from "react-redux";
import {postRestoQuality} from "../../store/actions/restaurantsActions";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: "200px",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  button:{
    margin: "10px",
    backgroundColor: "#1020ab",
    borderRadius: "10px",
  },
  btnTitle:{
    fontWeight: 'bold',
    fontSize: "18px",
    color: "#fff"
  }
}));

const CommentAndRating = ({onSubmit}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [state, setState] = useState({
    comment: '',
    scores1: '',
    scores2: '',
    scores3: '',
  });

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  const findScore = () => {
    const score = Math.round((state.scores1 + state.scores2 + state.scores3)/3);
    dispatch(postRestoQuality(state));
  };

  return (
      <form
          onSubmit={findScore}
          noValidate
          autoComplete="off"
      >
        <FormField
            label="Comment"
            name="comment"
            value={state.comment}
            onChange={inputChangeHandler}
            required={true}
        />
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">Quality of food</InputLabel>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={state.scores1}
              name="scores1"
              onChange={inputChangeHandler}
          >
            <MenuItem value={1}>Very bad</MenuItem>
            <MenuItem value={2}>Bad</MenuItem>
            <MenuItem value={3}>Norm</MenuItem>
            <MenuItem value={4}>Good</MenuItem>
            <MenuItem value={5}>Perfect</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">Service quality</InputLabel>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={state.scores2}
              name="scores2"
              onChange={inputChangeHandler}
          >
            <MenuItem value={1}>Very bad</MenuItem>
            <MenuItem value={2}>Bad</MenuItem>
            <MenuItem value={3}>Norm</MenuItem>
            <MenuItem value={4}>Good</MenuItem>
            <MenuItem value={5}>Perfect</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id="demo-simple-select-label">Interior</InputLabel>
          <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={state.scores3}
              name="scores3"
              onChange={inputChangeHandler}
          >
            <MenuItem value={1}>Very bad</MenuItem>
            <MenuItem value={2}>Bad</MenuItem>
            <MenuItem value={3}>Norm</MenuItem>
            <MenuItem value={4}>Good</MenuItem>
            <MenuItem value={5}>Perfect</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.button} variant="outlined">
          <Button color="primary" className={classes.btnTitle} type="submit">Submit Review</Button>
        </FormControl>
      </form>
  );
};

export default CommentAndRating;