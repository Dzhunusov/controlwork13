const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config");
const users = require("./app/users");
const restaurants = require("./app/restaurants");
const quality = require("./app/qualityRestos");
const gallery = require("./app/gallerys");
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
  await mongoose.connect(config.db.url + "/" + config.db.name,{useNewUrlParser: true, autoIndex: true});

  app.use("/users", users);
  app.use("/restaurants", restaurants);
  app.use("/quality", quality);
  app.use("/gallerys", gallery);


  console.log("Connected to mongo DB");
  app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
  })
};

run().catch(console.log);

