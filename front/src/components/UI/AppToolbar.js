import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import UserMenu from "../Menu/UserMenu";
import Anonymous from "../Menu/Anonymous";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  mainLink: {
    color: "inherit",
    textDecoration: "none",
    "&:hover": {
      color: "inherit",
    }
  },
  title: {
    flexGrow: 1,
  },
  dropdownBtn: {
    color: "#FFFFFF"
  },
  staticToolbar: {
    marginBottom: theme.spacing(2),
  },
}));

const AppToolbar = ({user}) => {
  const classes = useStyles();

  return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <Link to="/" className={classes.mainLink}>Resto Opinion</Link>
            </Typography>
            {user && user ? <UserMenu user={user} className={classes.dropdownBtn}/> : <Anonymous/>}
          </Toolbar>
        </AppBar>
        <Toolbar className={classes.staticToolbar}/>
      </div>
  );
};

export default AppToolbar;