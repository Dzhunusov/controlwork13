import React, {useState} from 'react';
import FormControl from "@material-ui/core/FormControl";
import FileInput from "../UI/Form/FileInput";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
}));

const AddNewImage = ({onSubmit}) => {
  const classes = useStyles();
  const [state, setState] = useState({image: ""});

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({...prevState, [name]: file}));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    const formData = new FormData();

    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });
    onSubmit(formData);
  };


  return (
      <form
          onSubmit={submitFormHandler}
          className={classes.root}
          noValidate
          autoComplete="off"
      >

        <FormControl fullWidth className={classes.margin} variant="outlined">
          <FileInput
              label="Image"
              name="image"
              onChange={fileChangeHandler}
          />
        </FormControl>
        <FormControl fullWidth className={classes.margin} variant="outlined">
          <Button color="primary" type="submit">Create</Button>
        </FormControl>
      </form>
  );
};

export default AddNewImage;