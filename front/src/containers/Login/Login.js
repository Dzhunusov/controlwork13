import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import {Link as RouterLink} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/actions/usersActions";
import Alert from '@material-ui/lab/Alert';


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert:{
    marginTop: theme.spacing(3),
    width: "100%"
  }
}));

const Login = () => {
  const classes = useStyles();
  const [state, setState] = useState({
    username: "",
    password: "",
  });
  const error = useSelector(state => state.users.loginError);
  const dispatch = useDispatch();

  const inputChangeHandler = e => {
    setState(prevState => ({...prevState, [e.target.name]: e.target.value}));
  };

  const submitHandler = e => {
    e.preventDefault();
    dispatch(loginUser({...state}));
  };


  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          {error && <Alert severity="error" className={classes.alert}>{error.error}</Alert>}
          <form className={classes.form} noValidate onSubmit={submitHandler}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                    autoComplete="username"
                    name="username"
                    variant="outlined"
                    fullWidth
                    value={state.username}
                    required={true}
                    onChange={inputChangeHandler}
                    id="username"
                    label="Username or email address"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    variant="outlined"
                    fullWidth
                    value={state.password}
                    onChange={inputChangeHandler}
                    required={true}
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                />
              </Grid>
            </Grid>
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link variant="body2" component={RouterLink} to="/register">
                  Or sign on
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
  )
};

export default Login;


