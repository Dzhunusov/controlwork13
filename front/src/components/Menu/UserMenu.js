import React from 'react';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import {useDispatch} from "react-redux";
import {logout} from "../../store/actions/usersActions";
import {Link} from "react-router-dom";

const UserMenu = ({user, className}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const dispatch = useDispatch();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logoutUser = () => {
    dispatch(logout());
  };


  return (
      <>
        <div>
          <Button
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={handleClick}
              className={className}
          >
            Hello, {user.username}!
          </Button>
          <Menu
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
          >
            <MenuItem>Profile</MenuItem>
            <MenuItem>My account</MenuItem>
            <MenuItem onClick={logoutUser}>Logout</MenuItem>
          </Menu>
        </div>
        <div>
          <Button component={Link} to="/new_place">
            Add new place
          </Button>
        </div>
      </>
  );
};

export default UserMenu;