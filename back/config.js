const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadsPath: path.join(rootPath, "public/uploads"),
  db: {
    name: "resto",
    url: "mongodb://localhost"
  }
};